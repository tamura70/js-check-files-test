function checkFile(path, list) {
    const xhr = new XMLHttpRequest();
    xhr.open("HEAD", path, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                console.log(xhr.statusText);
                list.insertAdjacentHTML('beforeend', `<li>${path}</li>`);
            } else {
                console.log(xhr.statusText);
                // list.insertAdjacentHTML('beforeend', `<li><strike>${path}</strike></li>`);
            }
        }
    };
    xhr.onerror = function (e) {
        console.error(xhr.statusText);
    };
    xhr.send(null);
}

function checkFiles() {
    const list = document.getElementById("list");
    list.innerHTML = "";
    const n = 100;
    for (let i = 1; i <= n; i++) {
        const path = `file-${i}.txt`;
        checkFile(path, list);
    }
}
